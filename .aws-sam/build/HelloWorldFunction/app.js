const HTTPS = require('https');
var AWS = require("aws-sdk");
var SNS = new AWS.SNS();

exports.handler = async (event, context) => {
    let dataString = '';
    
    const response = await new Promise((resolve, reject) => {
        const request = HTTPS.get("https://www.bankier.pl/", function(res) {
          res.on('data', chunk => {
            dataString += chunk;
          });
          res.on('end', () => {
            resolve({
                statusCode: 200,
                body: dataString
            });
          });
        });
        request.on('error', (e) => {
          reject({
              statusCode: 500,
              body: 'Something went wrong!'
          });
        });
    });
    
    if(response.body.includes("inflacja")){
        console.log('Inflacja wciaz na stronie glownej bankier.pl')
    } else {
      await publishSNS("Pierwszy raz nie ma żadnych informacji o rosnącej inflacji na Bankier", "arn:aws:sns:eu-central-1:1123123123123:BrakInformacjiOInflacji")
    }
    return response;
};

async function publishSNS(payload, topicArn) {
    await SNS.publish({
        Message: payload,
        TargetArn: topicArn
    }).promise().then((data) => {
        console.log('SNS push succeeded: ', data);
    }).catch((err) => {
        console.error(err);
    });
}